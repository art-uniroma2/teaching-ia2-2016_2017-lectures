package it.uniroma2.art.teaching.ia2_2016_2017.dudes.examples;

import de.citec.sc.dudes.rdf.RDFDUDES;

public class Example1A_married {
	public static void main(String[] args) {
		// --- Albert Einstein --- //

		RDFDUDES albertEinsteinDUDES =
			new RDFDUDES(RDFDUDES.Type.INDIVIDUAL);

		System.out.println(
			"An uninstantiated DUDES associated with a proper noun");
		System.out.println(albertEinsteinDUDES);
		System.out.println();

		albertEinsteinDUDES.instantiateIndividual(
			"http://dbpedia.org/resource/Albert_Einstein");

		System.out.println(
			"A DUDES associated with the proper noun Albert Einstein");
		System.out.println(albertEinsteinDUDES);
		System.out.println();

		// --- Elsa Einstein --- //

		RDFDUDES elsaEinsteinDUDES =
			new RDFDUDES(RDFDUDES.Type.INDIVIDUAL);
		elsaEinsteinDUDES.instantiateIndividual(
			"http://dbpedia.org/resource/Elsa_Einstein");

		System.out.println(
			"A DUDES associated with the proper noun Elsa Einstein");
		System.out.println(elsaEinsteinDUDES);
		System.out.println();

		// --- marry --- //
		RDFDUDES marryDUDES = new RDFDUDES(RDFDUDES.Type.PROPERTY, "subj", "dobj");
		System.out.println(
			"An uninstantiated DUDES associated with a subject and one argument");
		System.out.println(marryDUDES);
		System.out.println();

		System.out.println("A DUDES associated with the verb marry");
		marryDUDES.instantiateProperty(
			"http://dbpedia.org/ontology/spouse");
		System.out.println(marryDUDES);
		System.out.println();

		// --- Meaning composition --- //

		/* 
			@formatter:off
		 	
		 	S
		 		DP[subj]
		 			Albert Einstein
		 		VP
		 			V
		 				married
		 			DP[dobj]
		 				Elsa Einstein
		 				
		 	@formatter:on
		*/

		System.out.println("Albert Einstein married Elsa Einstein");
		System.out.println();
		
		RDFDUDES albertEinsteinMarriedDUDES =
			marryDUDES.merge(albertEinsteinDUDES, "subj");
		System.out.println("Composing the meaning of the subject");
		System.out.println(albertEinsteinMarriedDUDES);
		System.out.println();

		RDFDUDES albertEinsteinMarriedElsaEinsteinDUDES =
			albertEinsteinMarriedDUDES.merge(elsaEinsteinDUDES, "dobj");

		System.out.println("Composing the meaning of the subject");
		System.out.println(albertEinsteinMarriedElsaEinsteinDUDES);
		System.out.println();

		System.out.println("ASK Query (without post-processing)");
		System.out.println(albertEinsteinMarriedElsaEinsteinDUDES
			.convertToSPARQL(false));
		System.out.println();
		
		albertEinsteinMarriedElsaEinsteinDUDES.postprocess();

		System.out.println("ASK Query (with post-processing)");
		System.out.println(albertEinsteinMarriedElsaEinsteinDUDES
			.convertToSPARQL(false));
	}
}
