package it.uniroma2.art.teaching.ia2_2016_2017.dudes.examples;

import java.util.Arrays;

import de.citec.sc.dudes.Constant;
import de.citec.sc.dudes.DRS;
import de.citec.sc.dudes.DUDES;
import de.citec.sc.dudes.Proposition;
import de.citec.sc.dudes.Slot;
import de.citec.sc.dudes.Term;
import de.citec.sc.dudes.Variable;
import de.citec.sc.dudes.rdf.ExpressionFactory;
import de.citec.sc.dudes.rdf.RDFDUDES;

public class Example2_who_married {
	public static void main(String[] args) {
		ExpressionFactory expressions = new ExpressionFactory();

		RDFDUDES whoDUDES = expressions.what();

		System.out.println("DUDUES associated with Who");
		System.out.println(whoDUDES);
		System.out.println();
		
		RDFDUDES elsaEinsteinDUDES =
			new RDFDUDES(RDFDUDES.Type.INDIVIDUAL);
		elsaEinsteinDUDES.instantiateIndividual(
			"http://dbpedia.org/resource/Elsa_Einstein");

		System.out.println("DUDUES associated with Elsa Einstein");
		System.out.println(elsaEinsteinDUDES);
		System.out.println();
		
		DRS marryDRS = new DRS(0);
		Variable marryVar1 = new Variable(1);
		Variable marryVar2 = new Variable(2);
		Variable marryVar3 = new Variable(3);

		marryDRS.addStatement(new Proposition(marryVar1,
			Arrays.<Term> asList(marryVar2, marryVar3)));

		DUDES marryDUDESTemp = new DUDES();
		marryDUDESTemp.setDRS(marryDRS);
		marryDUDESTemp.setMainDRS(0);
		marryDUDESTemp.addSlot(new Slot(marryVar2, "subj", 0));
		marryDUDESTemp.addSlot(new Slot(marryVar3, "dobj", 0));
		RDFDUDES marriedDUDES =
			new RDFDUDES(marryDUDESTemp, RDFDUDES.Type.OTHER);

		marryDUDESTemp.replace(marryVar1,
			new Constant("http://dbpedia.org/ontology/spouse"));
		System.out.println("A DUDES associated with married");
		System.out.println(marriedDUDES);
		System.out.println();


		RDFDUDES whoMarriedDUDES = marriedDUDES.merge(whoDUDES, "subj");

		System.out.println("Who married ...");
		System.out.println(whoMarriedDUDES);
		System.out.println();
		
		RDFDUDES whoMarriedElsaEinsteinDUDES =
			whoMarriedDUDES.merge(elsaEinsteinDUDES, "dobj");

		System.out.println("Who married Elsa Einstein");
		System.out.println(whoMarriedElsaEinsteinDUDES);
		System.out.println();
		
		System.out.println("Query (without post-processing)");
		System.out
			.println(whoMarriedElsaEinsteinDUDES.convertToSPARQL());
		System.out.println();
		
		whoMarriedElsaEinsteinDUDES.postprocess();

		System.out.println("Query (with post-processing)");
		System.out
			.println(whoMarriedElsaEinsteinDUDES.convertToSPARQL());
		System.out.println();
	}
}
