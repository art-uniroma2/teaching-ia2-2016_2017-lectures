package it.uniroma2.art.teaching.ia2_2016_2017.dudes.examples;

import java.util.Arrays;

import de.citec.sc.dudes.Constant;
import de.citec.sc.dudes.DRS;
import de.citec.sc.dudes.DUDES;
import de.citec.sc.dudes.Proposition;
import de.citec.sc.dudes.Slot;
import de.citec.sc.dudes.Variable;
import de.citec.sc.dudes.rdf.ExpressionFactory;
import de.citec.sc.dudes.rdf.RDFDUDES;
import de.citec.sc.dudes.rdf.RDFDUDES.Type;

public class Example3_who_spouse_of {
	public static void main(String[] args) {
		ExpressionFactory expressions = new ExpressionFactory();

		// --- Who --- //

		RDFDUDES whoDUDES = expressions.what();

		System.out.println("DUDUES associated with Who");
		System.out.println(whoDUDES);
		System.out.println();

		// --- is (copula) --- //

		RDFDUDES copula = expressions.copula("1", "2");
		System.out.println("DUDES associated with copula");
		System.out.println(copula);
		System.out.println();

		// --- the --- //

		DRS theDRS = new DRS(0);
		Variable theVar1 = new Variable(1);
		theDRS.addVariable(theVar1);
		DUDES theDUDESTemp = new DUDES();
		theDUDESTemp.setDRS(theDRS);
		theDUDESTemp.setMainDRS(0);
		theDUDESTemp.setMainVariable(theVar1);
		theDUDESTemp.addSlot(new Slot(theVar1, "np"));

		RDFDUDES theDUDES = new RDFDUDES(theDUDESTemp, Type.OTHER);

		System.out.println("DUDUES associated with the");
		System.out.println(theDUDES);
		System.out.println();

		// --- spouse (of) --- //

		DRS spouseDRS = new DRS(0);
		Variable spouseVar1 = new Variable(1);
		Variable spouseVar2 = new Variable(2);
		spouseDRS.addStatement(new Proposition(
			new Constant("http://dbpedia.org/ontology/spouse"),
			Arrays.asList(spouseVar2, spouseVar1)));
		DUDES spouseDUDESTemp = new DUDES();
		spouseDUDESTemp.setDRS(spouseDRS);
		spouseDUDESTemp.setMainDRS(0);
		spouseDUDESTemp.setMainVariable(spouseVar1);
		spouseDUDESTemp.addSlot(new Slot(spouseVar2, "dp"));

		RDFDUDES spouseDUDES =
			new RDFDUDES(spouseDUDESTemp, Type.OTHER);

		System.out.println("DUDUES associated with spouse (of)");
		System.out.println(spouseDUDES);
		System.out.println();

		// --- Elsa Einstein --- //

		RDFDUDES elsaEinsteinDUDES =
			new RDFDUDES(RDFDUDES.Type.INDIVIDUAL);
		elsaEinsteinDUDES.instantiateIndividual(
			"http://dbpedia.org/resource/Elsa_Einstein");

		System.out.println("DUDUES associated with Elsa Einstein");
		System.out.println(elsaEinsteinDUDES);
		System.out.println();

		// --- Semantic composition --- //
		// Who is the spouse of Elsa Einstein? //

		RDFDUDES whoIsTheSposeOfElsaEinstein = copula
			.merge(whoDUDES, "1")
			.merge(theDUDES.merge(
					spouseDUDES.merge(elsaEinsteinDUDES, "dp"), "np"),
					"2");

		System.out.println("Who is the spouse of Elsa Einstein?");
		System.out.println(whoIsTheSposeOfElsaEinstein);
		System.out.println();

		System.out.println("Query (without post-processing)");
		System.out
			.println(whoIsTheSposeOfElsaEinstein.convertToSPARQL());
		System.out.println();

		whoIsTheSposeOfElsaEinstein.postprocess();

		System.out.println("Query (with post-processing)");
		System.out
			.println(whoIsTheSposeOfElsaEinstein.convertToSPARQL());
		System.out.println();
	}
}
