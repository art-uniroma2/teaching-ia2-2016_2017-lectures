package it.uniroma2.art.teaching.ia2_2016_2017.dudes.examples;

import de.citec.sc.dudes.rdf.ExpressionFactory;
import de.citec.sc.dudes.rdf.RDFDUDES;
import de.citec.sc.dudes.rdf.RDFDUDES.Type;

public class Example4_how_many_married {
	public static void main(String[] args) {
		ExpressionFactory expressions = new ExpressionFactory();

		// --- how many --- //

		RDFDUDES howmanyDUDES = expressions.howmany("np");

		System.out.println("DUDUES associated with how many");
		System.out.println(howmanyDUDES);
		System.out.println();

		// --- women --- //

		RDFDUDES womenDUDES = new RDFDUDES(Type.CLASS);
		womenDUDES.instantiateObject(
			"http://dbpedia.org/class/yago/Woman110787470");
		womenDUDES.instantiateProperty(
			"http://www.w3.org/1999/02/22-rdf-syntax-ns#type");

		System.out.println("DUDUES associated with women");
		System.out.println(womenDUDES);
		System.out.println();

		// --- Albert Einstein --- //

		RDFDUDES albertEinsteinDUDES =
			new RDFDUDES(RDFDUDES.Type.INDIVIDUAL);

		albertEinsteinDUDES.instantiateIndividual(
			"http://dbpedia.org/resource/Albert_Einstein");

		System.out.println("DUDUES associated with Albert Einstein");
		System.out.println(albertEinsteinDUDES);
		System.out.println();

		// --- married --- //

		RDFDUDES marryDUDES =
			new RDFDUDES(RDFDUDES.Type.PROPERTY, "subj", "dobj");
		System.out.println(marryDUDES);
		marryDUDES.instantiateProperty(
			"http://dbpedia.org/ontology/spouse");

		System.out.println("DUDUES associated with marry");
		System.out.println(marryDUDES);
		System.out.println();

		// --- did --- //

		RDFDUDES didDUDES = expressions.did();

		System.out.println("DUDUES associated with did");
		System.out.println(didDUDES);
		System.out.println();

		// --- Meaning composition --- //
		// How many women did Albert Einstein marry? //

		RDFDUDES howmanyWomenDUDES =
			howmanyDUDES.merge(womenDUDES, "np");

		// --- How many women --- //

		System.out.println("DUDES associated with how many women");
		System.out.println(howmanyWomenDUDES);
		System.out.println();

		// --- Albert Einstein marry --- //

		RDFDUDES albertEinsteinMarryDUDES =
			marryDUDES.merge(albertEinsteinDUDES, "subj");

		System.out
			.println("DUDES associated with Albert Einstein marry");
		System.out.println(albertEinsteinMarryDUDES);
		System.out.println();

		// --- How many women did Albert Einstein marry --- //
		
		RDFDUDES howManyWomenDidAlbertEinstenMarryDUDES =
			didDUDES.merge(albertEinsteinMarryDUDES
					.merge(howmanyWomenDUDES, "dobj"));

		System.out.println(
			"DUDES associated with How many women Albert Einstein marry");
		System.out.println(howManyWomenDidAlbertEinstenMarryDUDES);
		System.out.println();
		
		System.out.println("Query (without post-processing)");
		howManyWomenDidAlbertEinstenMarryDUDES.postprocess();
		System.out.println();
		
		System.out.println("Query (with post-processing)");
		System.out.println(
			howManyWomenDidAlbertEinstenMarryDUDES.convertToSPARQL());
		System.out.println();
	}
}
