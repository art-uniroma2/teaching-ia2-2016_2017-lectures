package it.uniroma2.art.teaching.ia2_2016_2017.dudes.examples;

import de.citec.sc.dudes.OperatorStatement.Operator;
import de.citec.sc.dudes.rdf.ExpressionFactory;
import de.citec.sc.dudes.rdf.RDFDUDES;
import de.citec.sc.dudes.rdf.RDFDUDES.Type;

public class Example5_highest_mountain {
	public static void main(String[] args) {
		ExpressionFactory expressions = new ExpressionFactory();

		// --- what --- //

		RDFDUDES whatDUDES = expressions.what();

		System.out.println("DUDES associated with what");
		System.out.println(whatDUDES);
		System.out.println();

		// --- is (copula) --- //

		RDFDUDES copulaDUDES = expressions.copula("1", "2");

		System.out.println("DUDES associated with copula");
		System.out.println(copulaDUDES);
		System.out.println();

		// --- the highest --- //

		RDFDUDES theHighestDUDES =
			expressions.superlative("np", Operator.MAX, true);
		theHighestDUDES.instantiateProperty(
			"http://dbpedia.org/ontology/prominence");

		System.out.println("DUDES associated with the highest");
		System.out.println(theHighestDUDES);
		System.out.println();

		// --- mountain --- //

		RDFDUDES mountainDUDES = new RDFDUDES(Type.CLASS);
		mountainDUDES.instantiateObject(
			"http://dbpedia.org/ontology/Mountain");
		mountainDUDES.instantiateProperty(
			"http://www.w3.org/1999/02/22-rdf-syntax-ns#type");

		System.out.println("DUDES associated with mountain");
		System.out.println(mountainDUDES);
		System.out.println();

		// --- meaning composition --- //

		RDFDUDES whatIsTheHighestMountain =
			copulaDUDES.merge(whatDUDES, "1").merge(
					theHighestDUDES.merge(mountainDUDES, "np"), "2");
		System.out.println("What is the highest mountain");
		System.out.println(whatIsTheHighestMountain);
		System.out.println();

		System.out.println("Query (without post-processing)");
		System.out
			.println(whatIsTheHighestMountain.convertToSPARQL());
		System.out.println();

		whatIsTheHighestMountain.postprocess();

		System.out.println("Query (with post-processing)");
		System.out
			.println(whatIsTheHighestMountain.convertToSPARQL());
		System.out.println();

	}
}
