package it.uniroma2.art.teaching.ia2_2016_2017.java8.examples;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Example1 {
	public static void main(String[] args) {
		// An action listener printing Hello, World!!!
		ActionListener listener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Hello, World!!!");
			}
		};

		// The code below creates a frame and then makes it visible. The
		// frame contains a button, to which the listener above is
		// added
		JButton button = new JButton("Press Me!");
		button.addActionListener(listener);
		JFrame frame = new JFrame("Example Button");
		frame.add(button);
		frame.pack();
		frame.setLocationRelativeTo(null); // centers the frame on the
											// screen
		// Makes the program exit, when the frame is closed
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true); // makes the frame visible
	}
}
