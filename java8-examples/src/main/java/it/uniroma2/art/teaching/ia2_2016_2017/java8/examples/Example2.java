package it.uniroma2.art.teaching.ia2_2016_2017.java8.examples;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Example2 {
	public static void main(String[] args) {
		// This example is equal to Example 1 except for the use of a
		// a lambda expression to define the action listener

		JButton button = new JButton("Press Me!");
		button.addActionListener(
			e -> System.out.println("Hello, World!!!"));
		JFrame frame = new JFrame("Example Button");
		frame.add(button);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
