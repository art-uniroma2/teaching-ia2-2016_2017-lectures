package it.uniroma2.art.teaching.ia2_2016_2017.java8.examples;

import java.util.HashMap;
import java.util.Map;

public class Example3 {
	public static void main(String[] args) {
		// Since Java 7, type arguments can be omitted from the
		// invocation of the constructor of a generic class, when
		// these arguments can be inferred from the context.

		// http://docs.oracle.com/javase/7/docs/technotes/guides/language/type-inference-generic-instance-creation.html

		Map<String, String> map = new HashMap<String, String>();
		Map<String, String> map2 = new HashMap<>(); // this pair of
													// angle brackets
													// is called
													// diamond
	}
}
