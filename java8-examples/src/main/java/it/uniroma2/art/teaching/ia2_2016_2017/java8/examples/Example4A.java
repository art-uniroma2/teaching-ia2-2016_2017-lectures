package it.uniroma2.art.teaching.ia2_2016_2017.java8.examples;

import java.util.function.Consumer;

public class Example4A {
	public static void main(String[] args) {
		new Example4A().myMethod();
	}

	public void myMethod() {
		int x = 1;
		// Since the local variable x is captured by an anonymous
		// class, it must be effectively final. Accordingly, the
		// (commented) statement below would cause a compile-time
		// error

		// x++;

		int y = 1;
		Consumer<Object> listener = new Consumer<Object>() {

			@Override
			public void accept(Object t) {
			// "this" is a reference to the instance of the anonymous
			// class
			System.out.println("this = " + this.getClass());
			// "Example4A.this" is a reference to the enclosing
			// object
			System.out.println(
					"Example4A.this = " + Example4A.this.getClass());
			// x is captured from the enclosing scope
			System.out.println("x is = " + x);
			int y = 0; // redeclares local variable y (therefore the
						// local variable defined in the enclosing
						// scope is not captured)
			System.out.println("y is = " + y);
			}
		};

		listener.accept(null);
	}
}
