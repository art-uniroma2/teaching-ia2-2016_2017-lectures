package it.uniroma2.art.teaching.ia2_2016_2017.java8.examples;

import java.util.function.Consumer;

public class Example4B {
	public static void main(String[] args) {
		new Example4B().myMethod();
	}

	public void myMethod() {
		int x = 1;
		// x++;
		int y = 1;

		Consumer<Object> listener = (o) -> {
			// "this" is a reference to the enclosing object
			System.out.println("this class is = " + this.getClass());
			// a lambda expression cannot redeclare a local variable
			// defined in an enclosing scope
			
			// int y = 0;
			
			System.out.println("x is = " + x);
			System.out.println("y is = " + y);
		};

		listener.accept(null);
	}
}
