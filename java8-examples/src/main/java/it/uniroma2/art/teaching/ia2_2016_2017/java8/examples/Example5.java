package it.uniroma2.art.teaching.ia2_2016_2017.java8.examples;

import java.util.Arrays;
import java.util.List;

public class Example5 {
	public static void main(String[] args) {
		List<Person> persons =
			Arrays.asList(new Person("Tony", "Hoare"),
					new Person("Albert", "Einstein"),
					new Person("Leslie", "Lamport"));

		System.out.println("original collection = " + persons);

		// Sorts a collection of persons wrt the order induced by the
		// comparator defined via a lambda expression
		persons.sort((p1, p2) -> { // note that the parameter types are
									// inferred from the context
			int c1 = p1.getFirstName().compareTo(p2.getFirstName());
			if (c1 != 0) {
				return c1;
			}
			return p1.getLastName().compareTo(p2.getLastName());
		});

		System.out.println("sorted collection = " + persons);

	}

}
