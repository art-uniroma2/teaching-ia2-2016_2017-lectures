package it.uniroma2.art.teaching.ia2_2016_2017.java8.examples;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Example6 {
	public static void main(String[] args) {
		List<Person> persons =
			Arrays.asList(new Person("Tony", "Hoare"),
					new Person("Albert", "Einstein"),
					new Person("Leslie", "Lamport"));

		System.out.println(persons);

		// Builds a comparator equivalent to the one used in Example 5
		// as the concatenation of two comparators, working on the
		// first name and the last name, respectively. These
		// comparators compare two objects by comparing their
		// associated sort keys (the key extraction function is
		// defined via a lambda expression), which must implement the
		// interface Comparable, unless a dedicated comparator is
		// specified.

		// @formatter:off
		/*
			persons.sort(Comparator
				.comparing((Person p) -> p.getFirstName()).thenComparing(
						(Person p) -> p.getLastName()));
		*/
		// @formatter:on

		// Method references can be used in place of a lambda
		// expression that just invokes one method of an object

		persons.sort(Comparator.comparing(Person::getFirstName)
			.thenComparing(Person::getLastName));

		System.out.println(persons);

	}

}