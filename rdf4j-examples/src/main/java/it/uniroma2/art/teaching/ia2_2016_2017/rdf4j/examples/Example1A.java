package it.uniroma2.art.teaching.ia2_2016_2017.rdf4j.examples;

import java.io.OutputStreamWriter;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;

public class Example1A {
	public static void main(String[] args) {
		String ns = "http://example.org/";
		
		ValueFactory vf = SimpleValueFactory.getInstance();
		IRI mortalIRI = vf.createIRI(ns, "Mortal");
		IRI personIRI = vf.createIRI(ns, "Person");
		IRI socratesIRI = vf.createIRI(ns, "socrates");
		
		Model model = new LinkedHashModel();
		
		model.add(mortalIRI, RDF.TYPE, RDFS.CLASS);
		model.add(mortalIRI, RDFS.LABEL, vf.createLiteral("Mortal", "en"));
		model.add(mortalIRI, RDFS.LABEL, vf.createLiteral("Mortale", "it"));
		
		model.add(personIRI, RDF.TYPE, RDFS.CLASS);
		model.add(personIRI, RDFS.LABEL, vf.createLiteral("Person", "en"));
		model.add(personIRI, RDFS.LABEL, vf.createLiteral("Persona", "it"));
		model.add(personIRI, RDFS.SUBCLASSOF, mortalIRI);

		model.add(socratesIRI, RDF.TYPE, personIRI);
		model.add(socratesIRI, RDFS.LABEL, vf.createLiteral("Socrates", "en"));
		model.add(socratesIRI, RDFS.LABEL, vf.createLiteral("Socrate", "it"));

		Rio.write(model, new OutputStreamWriter(System.out),
			RDFFormat.TURTLE);

	}
}
