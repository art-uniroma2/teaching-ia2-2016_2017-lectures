package it.uniroma2.art.teaching.ia2_2016_2017.rdf4j.examples;

import java.io.OutputStreamWriter;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;

public class Example1B {
	public static void main(String[] args) {
		ValueFactory vf = SimpleValueFactory.getInstance();
		ModelBuilder modelBuilder = new ModelBuilder();

		//@formatter:off
		Model model =
			modelBuilder
					.setNamespace("example", "http://example.org/")
					.subject("example:Mortal")
						.add("rdf:type", "rdfs:Class")
						.add("rdfs:label",
								vf.createLiteral("Mortal", "en"))
						.add("rdfs:label", vf.createLiteral("Mortale", "it"))
					.subject("example:Person")
						.add("rdf:type", RDFS.CLASS)
						.add("rdfs:label", vf.createLiteral("Person", "en"))
						.add("rdfs:label", vf.createLiteral("Persona", "it"))
						.add("rdfs:subClassOf", "example:Mortal")
					.subject("example:socrates")
						.add("rdf:type", "example:Person")
						.add("rdfs:label", vf.createLiteral("Socrates", "en"))
						.add("rdfs:label", vf.createLiteral("Socrate", "it"))
					.build();
		//@formatter:on

		Rio.write(model, new OutputStreamWriter(System.out),
			RDFFormat.TURTLE);

	}
}
