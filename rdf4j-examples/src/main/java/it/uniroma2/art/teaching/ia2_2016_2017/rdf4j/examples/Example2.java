package it.uniroma2.art.teaching.ia2_2016_2017.rdf4j.examples;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;

public class Example2 {
	public static void main(String[] args) throws RDFParseException,
			UnsupportedRDFormatException, IOException {
		Model model = Rio.parse(
			Example2.class.getResourceAsStream("example2.ttl"),
			"http://example.org/", RDFFormat.TURTLE);

		System.out.println("--------------\n");
		Rio.write(model, System.out, RDFFormat.RDFXML);
		System.out.println("\n--------------\n");

		Set<Resource> subjects = model.subjects();
		System.out.println(subjects);

		System.out.println("\n--------------\n");

		Set<String> subjects2 = subjects.stream()
			.filter(IRI.class::isInstance).map(IRI.class::cast)
			.map(IRI::getLocalName).collect(Collectors.toSet());
		System.out.println(subjects2);

		System.out.println("\n--------------\n");

		ValueFactory vf = SimpleValueFactory.getInstance();
		Resource socratesResource =
			vf.createIRI("http://example.org/Socrates");

		Set<Value> labels = model
			.filter(socratesResource, RDFS.LABEL, null).objects();

		System.out.println(labels);

	}
}
