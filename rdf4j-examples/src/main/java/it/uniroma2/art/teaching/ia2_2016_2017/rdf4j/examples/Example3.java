package it.uniroma2.art.teaching.ia2_2016_2017.rdf4j.examples;

import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.Sail;
import org.eclipse.rdf4j.sail.inferencer.fc.ForwardChainingRDFSInferencer;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

public class Example3 {
	public static void main(String[] args) {
		String ns = "http://example.org/";

		ValueFactory vf = SimpleValueFactory.getInstance();
		IRI mortalIRI = vf.createIRI(ns, "Mortal");
		IRI personIRI = vf.createIRI(ns, "Person");
		IRI socratesIRI = vf.createIRI(ns, "socrates");

		Sail sail =
			new ForwardChainingRDFSInferencer(new MemoryStore());
		Repository repo = new SailRepository(sail);
		try {
			repo.initialize();

			try (RepositoryConnection repoConn =
				repo.getConnection()) {
				repoConn.add(personIRI, RDF.TYPE, RDFS.CLASS);
				repoConn.add(mortalIRI, RDF.TYPE, RDFS.CLASS);
				repoConn.add(personIRI, RDFS.SUBCLASSOF, mortalIRI);
				repoConn.add(socratesIRI, RDF.TYPE, personIRI);
			}

			Repositories.consume(repo, repoConn -> {
				repoConn.export(
						Rio.createWriter(RDFFormat.TURTLE, System.out));
				});

			Set<Value> socratesTypes =
				Repositories.get(repo, repoConn -> {
					return QueryResults.asModel(
							repoConn.getStatements(socratesIRI,
									RDF.TYPE, null, true))
							.objects();
				});

			System.out.println(socratesTypes);
		} finally {
			repo.shutDown();
		}
	}
}