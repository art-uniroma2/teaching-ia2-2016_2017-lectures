package it.uniroma2.art.teaching.ia2_2016_2017.rdf4j.examples;

import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.sail.Sail;
import org.eclipse.rdf4j.sail.inferencer.fc.ForwardChainingRDFSInferencer;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

public class Example4 {
	public static void main(String[] args) {
		Sail sail =
			new ForwardChainingRDFSInferencer(new MemoryStore());
		Repository repo = new SailRepository(sail);
		try {
			repo.initialize();

			Repositories.consume(repo, repoConn -> {
				try {
					repoConn.add(
							Example4.class.getResource("example4-5.ttl"),
							null, RDFFormat.TURTLE);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
				TupleQuery query = repoConn.prepareTupleQuery(
						"SELECT ?x WHERE {?x <http://example.org/locatedIn> <http://example.org/Italy>}");
				query.setIncludeInferred(false);
				try (TupleQueryResult queryResults = query.evaluate()) {
					while (queryResults.hasNext()) {
						BindingSet solution = queryResults.next();
						System.out
								.println("## " + solution.getValue("x"));
					}
	
				}
			});
		} finally {
			repo.shutDown();
		}
	}
}