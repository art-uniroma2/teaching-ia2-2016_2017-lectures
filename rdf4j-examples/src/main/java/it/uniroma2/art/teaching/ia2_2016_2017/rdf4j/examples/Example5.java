package it.uniroma2.art.teaching.ia2_2016_2017.rdf4j.examples;

import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.query.parser.ParsedTupleQuery;
import org.eclipse.rdf4j.queryrender.builder.QueryBuilderFactory;
import org.eclipse.rdf4j.queryrender.sparql.SPARQLQueryRenderer;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.sail.Sail;
import org.eclipse.rdf4j.sail.inferencer.fc.ForwardChainingRDFSInferencer;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

public class Example5 {
	public static void main(String[] args) throws Exception {
		Sail sail =
			new ForwardChainingRDFSInferencer(new MemoryStore());
		Repository repo = new SailRepository(sail);
		try {
			repo.initialize();

			Repositories.consume(repo, repoConn -> {
				try {
					repoConn.add(
							Example4.class.getResource("example4-5.ttl"),
							null, RDFFormat.TURTLE);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			});

			ValueFactory vf = repo.getValueFactory();
			// @formatter:off
			ParsedTupleQuery parsedQuery =
				QueryBuilderFactory.select("x")
				                   .group()
						                .atom("x",
								                vf.createIRI("http://example.org/locatedIn"),
								                vf.createIRI("http://example.org/Italy")
							                  )
				                   .closeGroup()
				                   .query();
			// @formatter:on
			
			SPARQLQueryRenderer sparqlQueryRenderer =
				new SPARQLQueryRenderer();
			String queryString =
				sparqlQueryRenderer.render(parsedQuery);

			System.out.println(queryString);
			System.out.println("\n-------\n");
			Repositories.consume(repo, repoConn -> {
			TupleQuery tupleQuery =
					repoConn.prepareTupleQuery(queryString);
				try (TupleQueryResult queryResult =
						tupleQuery.evaluate()) {
					while (queryResult.hasNext()) {
						BindingSet bindingSet = queryResult.next();
						System.out.println(
								"## " + bindingSet.getValue("x"));
					}
				}
			});
		} finally {
			repo.shutDown();
		}
	}
}