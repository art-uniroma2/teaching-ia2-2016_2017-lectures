package it.uniroma2.art.teaching.ia2_2016_2017.rdf4j.examples;

import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;

public class Example6 {
	public static void main(String[] args) {
		SPARQLRepository repo =
			new SPARQLRepository("http://dbpedia.org/sparql");
		try {
			repo.initialize();
			try (RepositoryConnection repoConn =
				repo.getConnection()) {
				TupleQuery query = repoConn.prepareTupleQuery(
						"select ?x where {<http://dbpedia.org/resource/Albert_Einstein> <http://dbpedia.org/ontology/spouse> ?x}");
				try (TupleQueryResult queryResult = query.evaluate()) {
					while (queryResult.hasNext()) {
						System.out.println(
							"## " + queryResult.next().getValue("x"));
					}
				}
			}
		} finally {
			repo.shutDown();
		}
	}
}
