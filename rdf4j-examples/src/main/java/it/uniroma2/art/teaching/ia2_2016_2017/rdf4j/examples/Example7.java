package it.uniroma2.art.teaching.ia2_2016_2017.rdf4j.examples;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.sail.Sail;
import org.eclipse.rdf4j.sail.inferencer.fc.ForwardChainingRDFSInferencer;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

public class Example7 {
	public static void main(String[] args) {
		Sail sail =
			new ForwardChainingRDFSInferencer(new MemoryStore());
		Repository repo = new SailRepository(sail);
		try {
			repo.initialize();
			try (RepositoryConnection repoConn =
				repo.getConnection()) {
				ValueFactory vf = repoConn.getValueFactory();
				IRI resA = vf.createIRI("http://A");
				IRI resB = vf.createIRI("http://B");
				IRI resC = vf.createIRI("http://C");
	
				IRI g1 = vf.createIRI("http://g1");
				IRI g2 = vf.createIRI("http://g2");
	
				repoConn.add(resA, RDF.TYPE, OWL.CLASS);
				repoConn.add(resB, RDF.TYPE, OWL.CLASS, g1);
				repoConn.add(resC, RDF.TYPE, OWL.CLASS, g2);
	
				System.out.println(
						"Statements matching ?x rdf:type owl:Class in any context (w/o inference)");
	
				System.out.println(
						QueryResults.asList(repoConn.getStatements(null,
								RDF.TYPE, OWL.CLASS, false)));
	
				System.out.println();
				System.out.println("---");
				System.out.println();
	
				System.out.println(
						"Statements matching ?x rdf:type owl:Class in the null context (w/o inference)");
	
				System.out.println(QueryResults
						.asList(repoConn.getStatements(null, RDF.TYPE,
								OWL.CLASS, false, (Resource) null)));
	
				System.out.println();
				System.out.println("---");
				System.out.println();
	
				System.out.println(
						"Statements matching ?x rdf:type owl:Class in the context <http://g1> (w/o inference)");
	
				System.out.println(QueryResults.asList(
						repoConn.getStatements(null, RDF.TYPE, OWL.CLASS,
								false, vf.createIRI("http://g1"))));
	
				System.out.println();
				System.out.println("---");
				System.out.println();
	
				System.out.println(
						"Statements matching ?x rdf:type owl:Class in the null context (with inference)");
	
				System.out.println(QueryResults
						.asList(repoConn.getStatements(null, RDF.TYPE,
								OWL.CLASS, true, (Resource) null)));
	
				System.out.println(
						"/The ForwardChainingRDFSInferencer copies every statement into the null context/");
	
				System.out.println();
				System.out.println("---");
				System.out.println();
	
				System.out.println(
						"Statements matching ?x rdf:type owl:Class in any context (with inference)");
	
				System.out.println(QueryResults.asList(repoConn
						.getStatements(null, RDF.TYPE, OWL.CLASS, true)));
	
				System.out.println(
						"/Because of the beahavior of ForwardChainingRDFSInferencer, any triple in a named graph is repeated in the null context/");
	
				System.out.println();
				System.out.println("---");
				System.out.println();
	
				System.out.println(
						"SPARQL query matching ?x rdf:type owl:Class (w/o inference)");
	
				TupleQuery query = repoConn.prepareTupleQuery(
						"SELECT ?x WHERE { ?x a <http://www.w3.org/2002/07/owl#Class> . }");
				query.setIncludeInferred(false);
	
				System.out.println(QueryResults.asList(query.evaluate()));
				System.out.println(
						"/The graph pattern is matched (unless otherwise specified) against the default graph consisting of the RDF merge of the null context and every the named graphs/");
	
				System.out.println();
				System.out.println("---");
				System.out.println();
	
				System.out.println(
						"SPARQL query matching ?x rdf:type owl:Class (with inference)");
	
				TupleQuery query2 = repoConn.prepareTupleQuery(
						"SELECT ?x WHERE { ?x a <http://www.w3.org/2002/07/owl#Class> . }");
				query2.setIncludeInferred(true);
	
				System.out
						.println(QueryResults.asList(query2.evaluate()));
				System.out.println(
						"/Because of the beahavior of ForwardChainingRDFSInferencer, there should be two solutions for B and C/");
	
				System.out.println();
				System.out.println("---");
				System.out.println();
	
				System.out.println(
						"SPARQL query matching ?x rdf:type owl:Class againt graph <http://g1>");
	
				TupleQuery query3 = repoConn.prepareTupleQuery(
						"SELECT ?x WHERE { GRAPH <http://g1> { ?x a <http://www.w3.org/2002/07/owl#Class> . } }");
				query3.setIncludeInferred(true);
	
				System.out
						.println(QueryResults.asList(query3.evaluate()));
			}

		} finally {
			repo.shutDown();
		}
	}
}
